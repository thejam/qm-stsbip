// ==UserScript==
// @name         STSBITP Script
// @version      0.2.7
// @description  TamperMonkey that drastically decreases the time it takes to configure a subscription for QuantumMetric
// @author       JT - jtaylor@quantummetric.com
// @match        *://*/*
// @include      *
// @exclude      *.quantummetric.com*
// @grant        none
// @run-at       document-end
// @updateURL    https://gitlab.com/thejam/qm-stsbip/raw/master/qm-stsbitp.js
// @downloadURL  https://gitlab.com/thejam/qm-stsbip/raw/master/qm-stsbitp.js
// ==/UserScript==

//Turn on debugging (where supported)
var apiDebug = false;

var getQuantumSession = function(){
                return window.QuantumMetricAPI.getSession();
}

var addGlobalStyle = function(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}
addGlobalStyle('[cssblacklist="true"] { outline:2px dotted red; }');
addGlobalStyle('[regexblacklist="true"] { outline:2px dotted orange; }');
addGlobalStyle('[quantumclickevent="true"] { outline:2px dotted blue; }');
addGlobalStyle('[quantumlastclicked="true"] { outline:2px dotted purple; }');
addGlobalStyle('[cssencryptlist="true"] { outline:2px dotted green; }');
//addGlobalStyle('button.qmapibutton { all:initial; cursor: pointer;-webkit-appearance: button;font-size: 100%;margin: 0;vertical-align: baseline;line-height: normal;font-family: Roboto,Geneva,Tahoma,Arial,Helvetica Neue,Helvetica,sans-serif;color: #333;box-sizing: border-box;border-color: rgb(216, 216, 216) rgb(209, 209, 209) rgb(186, 186, 186);border-style: solid;border-width: 1px;padding: 1px 7px 2px;background-color: buttonface;-webkit-font-smoothing: antialiased;text-rendering: auto;color: initial;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;display: inline-block;text-align: start;margin: 0em;font: 400 11px system-ui;}');
//addGlobalStyle('button.qmapibutton { all:initial; }');

var getQMAPIData = function(request){
    if(request == 'cartValue'){
        return (window.QuantumMetricAPI.getCartValue()/100).toLocaleString("en-US", {style:"currency", currency:window.QuantumMetricAPI.targetCurrency});}
    if(request == 'cssblacklist'){
        return window.QuantumMetricAPI.getConfig().dataScrubBlackList;}
    if(request == 'quantumclickevents'){
        return window.QuantumMetricAPI.B.oe.events;}
    if(request == 'cssencryptlist'){
        return window.QuantumMetricAPI.getConfig().encryptScrubList;}
    if(request == 'lastClicked'){
        return window.QuantumMetricAPI.lastClicked;}
    if(request == 'regexblacklist'){
        return window.QuantumMetricAPI.B.Oc.toString();}
    if(request == 'spinner'){
        return window.QuantumMetricAPI.getConfig().spinnerSelectorList;}
    if(request == 'norage') {
        return window.QuantumMetricAPI.getConfig().excludeRageRE;}
}

var cssBlacklist = function(state){
    if(state == 'on'){
        var values=getQMAPIData('cssblacklist');
        var cssbl = "";
        values.forEach(function(item){
            cssbl = cssbl + item + ",";
        });
        cssbl = cssbl.substr(0,cssbl.length-1);
        
        //Adds a thin dotted red border to fields and other html elements that are being CSS Blacklisted
        for(var i=0, element=document.querySelectorAll(cssbl);i<element.length;i++){
            if(!!element[i].nodeName.match(/OPTION/) && !element[i].innerText.match(/\*QMBL\*/)){
                element[i].innerText = element[i].innerText + " *QMBL*";
            }
            element[i].setAttribute('cssblacklist', 'true');
        }
    }

    if(state == 'off'){
        for(var i=0, element=document.querySelectorAll('[cssblacklist="true"]');i<element.length;i++){
            if(!!element[i].nodeName.match(/OPTION/) && !!element[i].innerText.match(/\*QMBL\*/)){
                element[i].innerText = element[i].innerText.split(' *QMBL*')[0];
            }
            element[i].removeAttribute('cssblacklist');
        }
    }
}

var quantumClickEvents = function(state) {
    if(!!getQMAPIData('quantumclickevents')){
        if(state == 'on'){

            for(var c=0, clickcss = "", e=getQMAPIData('quantumclickevents');c<e.length;c++){
                var re = new RegExp(e[c].u, 'g');
                if(e[c].x == "QCE" && !!document.URL.match(re)){
                    if(!!apiDebug){console.log('found a click event on this Page - ' + e[c].v.v[0].v[1].v[0]);}
                    clickcss = clickcss + e[c].v.v[0].v[1].v[0] + ",";
                }
            }

            //Adds a medium dotted blue border to buttons and other html elements that are configured to be Quantum Click Events
            if(!!clickcss){
                for(var ce=0, QCEselector=document.querySelectorAll(clickcss.substr(0,clickcss.length-1));ce<QCEselector.length;ce++){
                    QCEselector[ce].setAttribute('quantumclickevent', 'true');
                }
            }

        }

        if(state == 'off'){
            if(document.querySelectorAll('[quantumclickevent="true"]').length > 0){
                for(var i=0, element=document.querySelectorAll('[quantumclickevent="true"]');i<element.length;i++){
                    element[i].removeAttribute('quantumclickevent');
                }
            }
        }
    }
}

var cssEncryptList = function(state) {
    if(state == 'on'){
        var values=getQMAPIData('cssencryptlist');
        var cssel = "";
        values.forEach(function(item){
            cssel = cssel + item + ",";
        });
        cssel = cssel.substr(0,cssel.length-1);
        
        for(var i=0, element=document.querySelectorAll(cssel);i<element.length;i++){
            if(!!element[i].nodeName.match(/OPTION/) && !element[i].innerText.match(/\*QMBL-Enc\*/)){
                element[i].innerText = element[i].innerText + " *QMBL-Enc*";
            }
            element[i].setAttribute('cssencryptlist', 'true');
        }
    }

    if(state == 'off'){
        for(var i=0, element=document.querySelectorAll('[cssencryptlist="true"]');i<element.length;i++){
            if(!!element[i].nodeName.match(/OPTION/) && !!element[i].innerText.match(/\*QMBL-Enc\*/)){
                element[i].innerText = element[i].innerText.split(' *QMBL-Enc*')[0];
            }
            element[i].removeAttribute('cssencryptlist');
        }
    }
}

var regexBlackList = function(state) {
    if(state == 'on'){
        var regexbl=getQMAPIData('regexblacklist');
        var l = regexbl.length;
        var re = new RegExp(regexbl.substr(1,l-2), 'g');
        var qsa = document.querySelectorAll('input');

        for(var i = 0;i < qsa.length;i++){
            var test = 0;

                if(!!qsa[i].name && !!qsa[i].name.match(re, 'g')){
                    test++;
                }
                if(!!qsa[i].id && !!qsa[i].id.match(re, 'g')){
                    test++;
                }
                if(!!qsa[i].className && !!qsa[i].className.match(re, 'g')){
                    test++;
                }
            if(test>0){
                qsa[i].setAttribute('regexblacklist', 'true');
                console.log(qsa[i]);
            }
        }

    }

    if(state == 'off'){
        for(var i=0, element=document.querySelectorAll('[regexblacklist="true"]');i<element.length;i++){
            element[i].removeAttribute('regexblacklist');
        }
    }
}


var qmSwitch = function(){
    var checked = this.checked;
    var id = this.id;

    if(!!checked){
        if(id == 'cssblacklistSwitch'){
            cssBlacklist('on');}
        if(id == 'clickeventsSwitch'){
            quantumClickEvents('on');}
        if(id == 'cssencryptSwitch'){
            cssEncryptList('on');}
        if(id == 'regexblacklistSwitch'){
            regexBlackList('on');}
    }else{
        if(id == 'cssblacklistSwitch'){
            cssBlacklist('off');}
        if(id == 'clickeventsSwitch'){
            quantumClickEvents('off');}
        if(id == 'cssencryptSwitch'){
            cssEncryptList('off');}
        if(id == 'regexblacklistSwitch'){
            regexBlackList('off');}
    }
}

var qmSessionControl = function(){
    var id = this.id;
    if(id == 'qmsessionstop'){
    window.QuantumMetricAPI.stopSession();
    console.log('stop session');}
    if(id == 'qmsessionstart'){
    window.QuantumMetricAPI.startSession();
    console.log('start session');}
    if(id == 'qmsessionnew'){
    window.QuantumMetricAPI.newSession();
    console.log('new session');}
    //setTimeout(sessionData(),500);
    }

var foo = function(){
    alert(this.id);
}

var getIntegrationPoints = function(){
    var intList = new Array();
    (!!window.newrelic) ? intList.push('New Relic') : false;
    (!!window.optimizely || checkLocalStorage("optimizely")) ? intList.push('Optimizely') : false;
    (!!checkCookies(';dtCookie=') || !!checkCookies('rxVisitor')) ? intList.push('Dynatrace') : false;
    (!!window.adobe && !!window.adobe.target) ? intList.push('Adode-target') : false;
    (!!window.utag_data || !!window.utag) ? intList.push('Tealium') : false;
    return intList;
}

var getCompetition = function(){
    var compList = new Array();
    (!!window._detector) ? compList.push('GlassBox') : false;
    (!!window.ClickTaleIsRecording || window.ClickTaleIsRecording()) ? compList.push('ClickTale') : false;
    (!!window.decibelInsight) ? compList.push('DecibelInsight') : false;
    (!!window.hj || !!window.hj.includedInSample) ? compList.push('HotJar') : false;
    (!!window.SessionCamRecorder) ? compList.push('SessionCam') : false;
    return compList;
}

var checkLocalStorage = function(pattern){
    var tf = false;
    Object.keys(window.localStorage).forEach(function(i){
        var re = new RegExp(pattern, 'g');
        (!!i.match(re)) ? tf = true : false;
    });
    return tf;
}

var checkCookies = function(pattern){
    var re = new RegExp(pattern, 'g');
    if(!!document.cookie.match(re)){
        return true;}
    else{
        return false;}
}

var isDescendant = function(parent, child) {
    if(child != null){
        var node = child.parentNode;
        while (node != null) {
            if (node == parent) {
                return true;
            }
            node = node.parentNode;
        }
    }
    return false;
}

var qmapiOverlay = function(){
    //QMAPIOverlay Header
    var header = document.createElement( 'div' );
    header.id = 'QMAPIOverlayheader';
    header.textContent = 'STSBITP Data Box';
    header.style.cssText = ''+
        'padding: 10px;'+
        'cursor: move;'+
        'z-index: 4001;'+
        'background-color: #2196F3;'+
        'color: #fff;'+
        'box-sizing: border-box;'+
        'font-family: Roboto,Geneva,Tahoma,Arial,Helvetica Neue,Helvetica,sans-serif;'+
        'font-size: 1em;'+
        'line-height: 1.4;'+
        'text-rendering: auto;'+
        '-webkit-font-smoothing: antialiased;';

    var minimizeButton = document.createElement('div');
    minimizeButton.id = 'minimizeQMPlugin';
    minimizeButton.textContent = '-'
    minimizeButton.style.cssText = 'position: fixed;'+
              'border: 1px solid white;'+
              'width: 2%;'+
              'margin-left: 0;'+
              'margin-top: -26px;'+
              'font-size: 22px;'+
              'font-weight: 800;'+
              'padding: 0;'+
              'cursor: pointer;';
    header.appendChild(minimizeButton);

    minimizeButton.addEventListener('click', function() {
      var wrapper = document.querySelector('#qmPlugWrapper');
      if(wrapper.style.visibility == 'inherit') {
        wrapper.style.visibility = 'hidden';
        wrapper.style.height = '0';
      } else {
        wrapper.style.visibility = 'inherit';
        wrapper.style.height = '100%';
      }
    });

    // container div for content
    var container = document.createElement('div');
    container.id = 'qmPlugWrapper'
    container.style.cssText = 'visibility: inherit;'

    //QMAPIOverlay Entire Box
    var box = document.createElement('div');
    box.id = 'QMAPIOverlay';
    box.style.cssText = 'all: initial;'+
        'position: absolute;'+
        'z-index: 4000;'+
        'top: '+
         (!!window.localStorage.getItem('qmOverlayTop') ? window.localStorage.getItem('qmOverlayTop') : '8px')+
        '; '+
        'left:'+
        (!!window.localStorage.getItem('qmOverlayLeft') ? window.localStorage.getItem('qmOverlayLeft') : '8px')+
        ';'+
        'background-color: #f1f1f1;'+
        'border: 1px solid #d3d3d3;'+
        'text-align: center;'+
        'font-family: Roboto,Geneva,Tahoma,Arial,Helvetica Neue,Helvetica,sans-serif;'+
        'box-sizing: border-box;'+
        'color: #333;'+
        'font-size: 1em;'+
        'line-height: 1.4;'+
        'text-rendering: auto;'+
        '-webkit-font-smoothing: antialiased;';

    var qmdata = document.createElement('div');
    qmdata.id = 'qmdata';
    qmdata.style.cssText = 'text-align: left; padding: 8px;';

    var qmoptions = document.createElement('div');
    qmoptions.id = 'qmoptions';
    qmoptions.style.cssText = 'text-align: left; padding: 8px;';

    var qmsession = document.createElement('div');
    qmsession.id = 'qmsession';
    qmsession.style.cssText = 'text-align: left; padding: 8px;';
    qmsession.innerHTML = '<a target="_blank" id="qmsessionlink"><span id="qmsessionid"></span></a>';

    var qmsessioncontrol = document.createElement('div');
    qmsessioncontrol.id = 'qmsessioncontrol';
    qmsessioncontrol.style.cssText = 'text-align: left; padding: 8px;';
    qmsessioncontrol.innerHTML =
        '<button class="qmapibutton" id="qmsessionstop"></button>'+
        '<button class="qmapibutton" id="qmsessionstart"></button>'+
        '<button class="qmapibutton" id="qmsessionnew">New Session</button>';

    var qmcartvalue = document.createElement('span');
    qmcartvalue.id = 'qmcartvalue';
    qmcartvalue.class = 'qmdata';
    qmcartvalue.style.cssText = 'text-align: left; padding: 8px;';


    var createOption = function(id, where, what, color){
        var input = document.createElement('input');
        input.type = 'checkbox';
        input.checked = true;
        input.id = id;
        input.class = "qmapiswitch"
        input.style.cssText = '-webkit-appearance: checkbox;padding: 8px;';

        var label = document.createElement('label');
        label.setAttribute('for', id);
        label.innerText = what + ' ';
        //label.style.cssText = 'display: inline;padding: 8px;';

        var legend = document.createElement('div');
        legend.style.cssText = 'outline:2px dotted '+color+';margin: 8px;';

        legend.appendChild(input);
        legend.appendChild(label);
        where.appendChild(legend);
        //where.appendChild(document.createElement('br'));
    }

    var qmintegrationpoints = document.createElement('div');
    qmintegrationpoints.id = 'qmintegrationpoints';
    qmintegrationpoints.style.cssText = 'text-align: left; padding: 8px;';

    //Build the overlay
    document.body.appendChild(box);
    box.insertBefore(header, box.firstChild);

    container.appendChild(qmdata);
    qmdata.appendChild(qmcartvalue);
    container.appendChild(qmoptions);
    container.appendChild(qmsession);
    container.appendChild(qmsessioncontrol);
    container.appendChild(qmintegrationpoints);

    box.appendChild(container);

    //Generate Options
    createOption('cssblacklistSwitch', qmoptions, 'CSS Blacklist', 'red');
    createOption('regexblacklistSwitch', qmoptions, 'Regex Blacklist', 'orange');
    createOption('clickeventsSwitch', qmoptions, 'Click Events', 'blue');
    createOption('lastclickedSwitch', qmoptions, 'Last Clicked', 'purple');
    createOption('cssencryptSwitch', qmoptions, 'CSS Encrypt', 'green');

    for(var i=0, s=getIntegrationPoints();i<s.length;i++){
        var integration = document.createElement('span');
        integration.id = s[i];
        integration.innerText = s[i];
        integration.class = 'qmintegration';
        integration.style.cssText = 'padding: 8px;';
        document.getElementById('qmintegrationpoints').appendChild(integration);
    };

    document.getElementById('cssblacklistSwitch').addEventListener('change', qmSwitch, false);
    document.getElementById('regexblacklistSwitch').addEventListener('change', qmSwitch, false);
    document.getElementById('clickeventsSwitch').addEventListener('change', qmSwitch, false);
    document.getElementById('cssencryptSwitch').addEventListener('change', qmSwitch, false);

    document.getElementById('qmsessionstop').addEventListener('click', qmSessionControl, false);
    document.getElementById('qmsessionstart').addEventListener('click', qmSessionControl, false);
    document.getElementById('qmsessionnew').addEventListener('click', qmSessionControl, false);




    // Make the DIV element draggable:
    dragElement(document.getElementById("QMAPIOverlay"));

    function dragElement(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(elmnt.id + "header")) {
            // if present, the header is where you move the DIV from:
            document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
        } else {
            // otherwise, move the DIV from anywhere inside the DIV:
            elmnt.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
            window.localStorage.setItem('qmOverlayTop', elmnt.style.top);
            window.localStorage.setItem('qmOverlayLeft', elmnt.style.left);
        }

        function closeDragElement() {
            // stop moving when mouse button is released:
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }
}

var timers = function(){
    var waitSessionStatus = setInterval(function(){
        var parts = document.cookie.split('QuantumMetricEnabled=');
        if(parts.length == 2){
            var qmEnabled = parts.pop().split(';').shift();
            if(qmEnabled == 'false'){
                document.getElementById('qmsessionstop').innerText = 'Session Off';
                document.getElementById('qmsessionstop').style = 'color: red;';
                document.getElementById('qmsessionstart').innerText = 'Start Session';
                document.getElementById('qmsessionstart').removeAttribute('style');
            }else if(qmEnabled == 'true'){
                document.getElementById('qmsessionstop').innerText = 'Stop Session';
                document.getElementById('qmsessionstop').removeAttribute('style');
                document.getElementById('qmsessionstart').innerText = 'Session On';
                document.getElementById('qmsessionstart').style = 'color: green;';
            }
        }else if(!!getQuantumSession()){
            document.getElementById('qmsessionstop').innerText = 'Stop Session';
            document.getElementById('qmsessionstop').removeAttribute('style');
            document.getElementById('qmsessionstart').innerText = 'Session On';
            document.getElementById('qmsessionstart').style = 'color: green;';
        }
    }, 2000);

    var waitForCartValue = setInterval(function(){
        if(getQMAPIData('cartValue') != '$0.00' && getQMAPIData('cartValue') != '$NaN' && getQMAPIData('cartValue') != document.getElementById('qmcartvalue').innerText.split(' ')[2]){
            document.getElementById('qmcartvalue').innerText = 'cartValue: ' + getQMAPIData('cartValue');
        }
    },500);

    var waitForLastClicked = setInterval(function(){
        if(typeof getQMAPIData('lastClicked') === 'object'){
            if(!isDescendant(document.getElementById('QMAPIOverlay'),getQMAPIData('lastClicked'))){
                if(document.querySelector('[quantumlastclicked="true"]') != null){
                    document.querySelector('[quantumlastclicked="true"]').removeAttribute('quantumlastclicked');}
                getQMAPIData('lastClicked').setAttribute('quantumlastclicked', 'true');
            }
        }
    },500);

    var waitForSessionCookie = setInterval(function(){
        if(document.cookie.split('QuantumMetricSessionID=').length == 2){
            var qmCookie = getQuantumSession();
            if(qmCookie != document.getElementById('qmsessionid').innerText){
                document.getElementById('qmsessionid').innerText = qmCookie;
                document.getElementById('qmsessionlink').href = window.QuantumMetricAPI.getConfig().reportURL.split('-')[0] + '.quantummetric.com/#/users/search?qmsessioncookie=' + qmCookie;
            }
        }
    }, 1000);
}

var waitForQMAPI = setInterval(function() {
    //START of wait for QMAPI
    if(!!window.QuantumMetricAPI) {
        clearInterval(waitForQMAPI);

        cssBlacklist('on');
        quantumClickEvents('on');
        cssEncryptList('on');
        regexBlackList('on');
        qmapiOverlay();
        timers();

    }
}, 250);
